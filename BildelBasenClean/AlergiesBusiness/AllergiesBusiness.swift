//
//  AllergiesBusiness.swift
//  BildelBasenClean
//
//  Created by Apple on 10/17/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

class AllergiesBusiness {
    // MARK: - UserServices
    private lazy var allergiesServices = AllergiesServices()

    // MARK: - Login

    func getAllAllergies(parameters: [String : Any],completion:@escaping((_ message:AllergiesModelList?,_ error:Error?) -> ())){

        allergiesServices.allergiesList(parameters: parameters) { (allergiesList, error) in
            if error == nil{
                guard let allergiesList = allergiesList else {return}
                guard let result = allergiesList.result else {return}
                guard let allergies = result.first else {return}
                completion(allergiesList,error)
                //                guard let serverUser = userModel?.result?["user"] else{return}
                //                let user = User.init(serverUser: serverUser, completed: true)
                //                print(user.firstname)
            }else{

            }

        }
    }




    // MARK: - UpdateProfile





}
