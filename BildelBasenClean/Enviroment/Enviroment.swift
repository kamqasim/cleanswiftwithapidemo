//
//  Enviroment.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

enum Environment {

    case development
    case staging
    case production

    func baseURL() -> String {
        return "\(urlProtocol())://\(subdomain()).\(domain())\(route())"
       // return "http://api.rikskampen.se/public/api"
    }

    func urlProtocol() -> String {
        switch self {
        case .production:
            return "https"
        default:
            return "http"
        }
    }

    func domain() -> String {
        switch self {
        case .development, .staging, .production:
            return "rikskampen.se"
        }
    }

    func subdomain() -> String {
        switch self {
        case .development:
            return "api"
        case .staging:
            return "test.subdomain"
        case .production:
            return "prod.subdomain"
        }
    }

    func route() -> String {
        return "/public/api"
    }

}

extension Environment {
    func host() -> String {
        return "\(self.subdomain()).\(self.domain())"
    }
}


// MARK:- APIs

#if DEBUG
let environment: Environment = Environment.development
#else
let environment: Environment = Environment.staging
#endif

let baseUrl = environment.baseURL()

struct Path {
    struct User {

        var registration: String { return "\(baseUrl)/registration" }
        var login: String { return "\(baseUrl)/userlogin" }
        var forgotPassword: String { return "\(baseUrl)/forgotPassword" }
        var logout: String { return "\(baseUrl)/logout" }
        var getProfile: String { return "\(baseUrl)/profile" }
        var deleteUser: (Int) -> String = { userID in
            return "\(baseUrl)/profile/\(String(userID))"
        }

        struct Task {
            var getTasks: String { return "\(baseUrl)/tasks" }
            var getTaskDetail: (Int, Int) -> String = { userID, taskID in
               // return "\(baseUrl)/profile/\(String(userID))/task/\(String(taskID))"
                return "\(baseUrl)"
            }
        }
    }

    struct Allergy {
        var getAllAlergies: String { return "\(baseUrl)/allergies" }
    }
}

/*

 baseUrl

 Path().login

 Path.User().getProfile

 Path.User().deleteUser(525)

 Path.User.Task().getTaskDetail(525, 2)

 */
