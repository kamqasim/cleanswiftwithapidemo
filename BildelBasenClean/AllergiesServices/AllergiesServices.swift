//
//  AllergiesBussiness.swift
//  BildelBasenClean
//
//  Created by Apple on 10/17/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

import Foundation

class AllergiesServices {


    // MARK: - UserAllergies

    func allergiesList(parameters: [String: Any], completion: @escaping (AllergiesModelList?, Error?) -> ()) {
         // api
        let api = AllergiesApiHandler()
        // api loader
        let apiTaskLoader = APILoader(apiRequest: api)

        apiTaskLoader.loadAPIRequest(requestData: parameters) { (result:AllergiesModelList?, error:Error?) in
            guard let allergiesList = result else {return}
            guard let result = allergiesList.result?.values else {return}
            print(result.first)
            completion(allergiesList, error)
        }
    }
}
