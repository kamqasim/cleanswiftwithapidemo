//
//  ErrorType.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import Foundation

// MARK: - Errors

struct ServiceError: Error, Codable {
    let httpStatus: Int
    let message: String
    let description: String?
}

struct NetworkError: Error {
    let message: String
}

struct UnknownParseError: Error { }
