//
//  ApiLoader.swift
//  BildelBasenClean
//
//  Created by Apple on 10/16/19.
//  Copyright © 2019 Apple. All rights reserved.
//


import Foundation



class APILoader<T: APIHandler> {

    let apiRequest: T
    let urlSession: URLSession
    let reachibility: Reachability

    init(apiRequest: T, urlSession: URLSession = .shared, reachibility: Reachability = Reachability()!) {
        self.apiRequest = apiRequest
        self.urlSession = urlSession
        self.reachibility = reachibility
    }

    func loadAPIRequest(requestData: T.RequestDataType,
                        completionHandler: @escaping (T.ResponseDataType?, Error?) -> ()) {
        // check network status
        if reachibility.connection == .none {
            return completionHandler(nil, NetworkError(message: "No Internet Connection"))
        }
        // prepare url request
        var urlRequest = apiRequest.makeRequest(from: requestData).urlRequest
        urlSession.dataTask(with: urlRequest) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do {
                let parsedResponse = try self.apiRequest.parseResponse(data: dataResponse)

                return completionHandler(parsedResponse, nil)
            } catch let parsingError {
                return completionHandler(nil, parsingError)
            }
        }.resume()
    }
}

