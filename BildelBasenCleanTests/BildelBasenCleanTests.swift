//
//  BildelBasenCleanTests.swift
//  BildelBasenCleanTests
//
//  Created by Apple on 10/15/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import XCTest
@testable import BildelBasenClean

class BildelBasenCleanTests: XCTestCase {


    // MARK: - Subject under test

     var sut: Greeting!
     var email: String?
     var password: String?

    var worker :LoginVCWorker?

   override func setUp() {
            // Put setup code here. This method is called before the invocation of each test method in the class.
            super.setUp()
            setUpWorker()
               email = nil
               password = nil
        }

        override func tearDown() {
            // Put teardown code here. This method is called after the invocation of each test method in the class.
            super.tearDown()
        }
         // MARK: - Test setup
        func setUpWorker(){
            worker = LoginVCWorker()
        }

        // MARK: - Test setup



        func testHasOneSection() {
            let dataSource = MenuDataSource()
            XCTAssertEqual(dataSource.numberOfSections, 1)

        }

        func testEmailValidation()
        {
            let email = "a"

            XCTAssertTrue((worker?.isValidEmail(email: email)) ?? false)
        }


        func testPAsswordValidation()
        {
            let password = "a"

            XCTAssertTrue((worker?.isValidPassword(testStr: password)) ?? false)
        }


        func testGenerateShouldReturnNilWithNilFirstAnd()
            {
              // Given

              // When
                let validate = worker?.checkEmpty(email:email,password:password)
              // Then
              XCTAssertNil(validate, "generate(email:password:) should return nil without email and password ")
            }
         // MARK: - Test doubles

         // MARK: - Tests

         func testGenerateShouldReturnNilWithNilFirstAndLastName()
         {
           // Given

           // When
           let greeting =  worker?.checkEmpty(email:email,password:password)

           // Then
           XCTAssertNil(greeting, "generate(firstName:lastName:) should return nil without first and last names")
         }

         func testGenerateShouldReturnFriendlyGreetingWithJustFirstName()
         {
           // Given
           email = "kamqasim1@gmail.com"

           // When
           let greeting = worker?.checkEmpty(email:email,password:password)
            print(greeting)
           // Then
           XCTAssertEqual(greeting, "Hi Raymond.", "checkEmpty(email:password:) should return a friendly greeting with just the email ")
         }

         func testGenerateShouldReturnFormalGreetingWithJustLastName()
         {
           // Given
           password = "12345678"

           // When
           let greeting = worker?.checkEmpty(email:email,password:password)
            print(greeting)
           // Then
           XCTAssertEqual(greeting, "Hello, Mr. Law.", "generate(firstName:lastName:) should return a formal greeting with just the last name")
         }

         func testGenerateShouldReturnFullGreetingWithBothFirstAndLastName()
         {
           // Given
                email = "kamqasim1@gmail.com"
                password = "12345678"
           // When
           let greeting = worker?.checkEmpty(email:email,password:password)
            print(greeting)
           // Then
           XCTAssertEqual(greeting, "Good to see you, Mr. Raymond Law.", "generate(firstName:lastName:) should return a full greeting with both first and last names")
         }

    }

    struct MenuDataSource {

        let numberOfSections = 1
    }
